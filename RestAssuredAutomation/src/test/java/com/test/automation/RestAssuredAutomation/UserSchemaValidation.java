package com.test.automation.RestAssuredAutomation;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static io.restassured.module.jsv.JsonSchemaValidator.*;

public class UserSchemaValidation {
	
	final String baseURI = "https://jsonplaceholder.typicode.com";
	
	@Test
	public void testUserSchema() {
		get(baseURI + "/users").then().assertThat()
								.body(matchesJsonSchemaInClasspath("users-schema.json"));
	}

}
