package com.test.automation.RestAssuredAutomation;

import org.testng.annotations.Test;
import io.restassured.http.ContentType;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class UsersApiTest {
	
	final String baseURI = "https://jsonplaceholder.typicode.com";
	
	@Test
	public void user_01_status_200() {
		System.out.println("user_01");
		given().get(baseURI + "/users")
				.then()
				.statusCode(200)
				.contentType(ContentType.JSON);
		
	}
	
	@Test(dependsOnMethods = { "user_01_status_200" })
	public void user_02_body_test() {
		given().get(baseURI + "/users")
			.then()
			.body("username", hasItems("Bret", "Antonette", "Samantha", "Karianne", "Kamren", "Leopoldo_Corkery", "Elwyn.Skiles",
										"Maxime_Nienow", "Delphine", "Moriah.Stanton"));
			
	}
	
	@Test(dependsOnMethods = { "user_01_status_200", "user_02_body_test"})
	public void user_03_todo_test() {
		given().get(baseURI + "/users/1/todos")
		.then()
		.body("userId", hasItem(1));
	}

}
